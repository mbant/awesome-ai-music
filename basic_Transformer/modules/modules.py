import torch
from torch import nn
import torch.nn.functional as F

import random, math


def mask_(matrices, maskval=0.0, mask_diagonal=True):
    """
    Masks out all values in the given batch of matrices where i <= j holds,
    i < j if mask_diagonal is false
    In place operation
    :param tns:
    :return:
    """

    b, h, w = matrices.size()

    indices = torch.triu_indices(h, w, offset=0 if mask_diagonal else 1)
    matrices[:, indices[0], indices[1]] = maskval


class RelativeSelfAttentionShaw(nn.Module):
    def __init__(self, emb, heads=8, mask=False, max_relative_position):
        """
        :param emb:
        :param heads:
        :param mask:
        """

        super().__init__()

        self.emb = emb
        self.heads = heads
        self.mask = mask

        self.tokeys = nn.Linear(emb, emb * heads, bias=False)
        self.toqueries = nn.Linear(emb, emb * heads, bias=False)
        self.tovalues = nn.Linear(emb, emb * heads, bias=False)
        
        self.Ak = Parameter(torch.empty(max_relative_position * 2 + 1, emb))
        self.Av = Parameter(torch.empty(max_relative_position * 2 + 1, emb))
        
        nn.init.normal_(self.Ak, mean=0, std=emb ** -0.5)
        nn.init.normal_(self.Av, mean=0, std=emb ** -0.5)
        
        self.o = nn.Linear(heads * emb, emb)

    def forward(self, x):

        b, t, e = x.size()
        h = self.heads
        assert e == self.emb, f'Input embedding dim ({e}) should match layer embedding dim ({self.emb})'

        # Compute Q K V for all heads
        queries = self.toqueries(x).view(b, t, h, e)
        keys    = self.tokeys(x)   .view(b, t, h, e)
        values  = self.tovalues(x) .view(b, t, h, e)

        # compute scaled dot-product self-attention
        # - fold heads into the batch dimension
        keys = keys.transpose(1, 2).contiguous().view(b * h, t, e)
        queries = queries.transpose(1, 2).contiguous().view(b * h, t, e)
        values = values.transpose(1, 2).contiguous().view(b * h, t, e)
        
        # Compute the relative-distance representations for keys and values
        #relations_keys = subset and broadcast Ak so that we get a correct [t, t, e] matrix
                        # for each i in range(t) we have to gather the matrix Ak[t-i, 2*t-i+1]
                        # (not considering clipping)
        relation_keys = [ Ak[(t-i):(2*t-i+1), :] for i in range(t) ] 
            # then reshape this into a tensor [t, t, e] (each is [t, e])
        
        #relations_values = same as above but for Av
        
        ### Compute attention
        queries = queries / (e ** (1/4))
        keys    = keys / (e ** (1/4))
        # - Instead of dividing the dot products by sqrt(e), we scale the keys and values.
        #   This should be more memory efficient

        # - get dot product of queries and keys
        dot = torch.bmm(queries, keys.transpose(1, 2))
        
        #### HERE IS WHERE WE DIVERGE -- 
        qq = queries.transpose(0,1) # [b*h, t, e] -> [t, b*h, e]
        # we need A size: [t, e, t] so use transpose
        # batch (of size t) matmul
        dot2 = torch.bmm(qq, relation_keys.transpose(1,2) / (e ** (1/4)) )
        # remember to scale the last term by (e** (1/4)) as well, since Q is already scaled!
        # dot2 has size [t, b*h, t]
        
        #  [t, b*h, t] -> [b*h, t, t]
        dot2.transpose(0,1)
        
        ## add them together
        dot = dot + dot2 
        assert dot.size() == (b*h, t, t)

        if self.mask: # mask out the upper half of the dot matrix, excluding the diagonal
            mask_(dot, maskval=float('-inf'), mask_diagonal=False)

        dot = F.softmax(dot, dim=2)
        # - dot now has row-wise RELATIVE self-attention probabilities
        # and size: [b*h, t, t]
        
        # apply the self attention to the values
        out = torch.bmm(dot, values)

        ### HERE WE DIVERGE AGAIN
        # out has size: [b*h, t, e]
        # dot has size: [b*h, t, t]
        # relations_values has size [t, t, e]
        
         # make dot [t, b*h, t], bmm with r_v [t, t, e] -- > [t, b*h, e]
        out2 = torch.bmm(dot.transpose(0,1), relation_values)
        
        out = out + out2.transpose(0,1)
        assert out.size() == (b*h, t, e)

        # swap h, t back and unify heads
        out = out.view(b, h, t, e).transpose(1, 2).contiguous().view(b, t, h * e)

        return self.o(out)
    
    
class SelfAttention(nn.Module):
    def __init__(self, emb, heads=8, mask=False):
        """
        :param emb:
        :param heads:
        :param mask:
        """

        super().__init__()

        self.emb = emb
        self.heads = heads
        self.mask = mask

        self.tokeys = nn.Linear(emb, emb * heads, bias=False)
        self.toqueries = nn.Linear(emb, emb * heads, bias=False)
        self.tovalues = nn.Linear(emb, emb * heads, bias=False)

        self.o = nn.Linear(heads * emb, emb)

    def forward(self, x):

        b, t, e = x.size()
        h = self.heads
        assert e == self.emb, f'Input embedding dim ({e}) should match layer embedding dim ({self.emb})'

        # Compute Q K V for all heads
        queries = self.toqueries(x).view(b, t, h, e)
        keys    = self.tokeys(x)   .view(b, t, h, e)
        values  = self.tovalues(x) .view(b, t, h, e)

        # compute scaled dot-product self-attention
        # - fold heads into the batch dimension
        keys = keys.transpose(1, 2).contiguous().view(b * h, t, e)
        queries = queries.transpose(1, 2).contiguous().view(b * h, t, e)
        values = values.transpose(1, 2).contiguous().view(b * h, t, e)

        queries = queries / (e ** (1/4))
        keys    = keys / (e ** (1/4))
        # - Instead of dividing the dot products by sqrt(e), we scale the keys and values.
        #   This should be more memory efficient

        # - get dot product of queries and keys
        dot = torch.bmm(queries, keys.transpose(1, 2))

        assert dot.size() == (b*h, t, t)

        if self.mask: # mask out the upper half of the dot matrix, excluding the diagonal
            mask_(dot, maskval=float('-inf'), mask_diagonal=False)

        dot = F.softmax(dot, dim=2)
        # - dot now has row-wise self-attention probabilities

        # apply the self attention to the values
        out = torch.bmm(dot, values).view(b, h, t, e)

        # swap h, t back and unify heads
        out = out.transpose(1, 2).contiguous().view(b, t, h * e)

        return self.o(out)
    
    
class TransformerBlock(nn.Module):
    def __init__(self, emb, heads, mask, seq_length, ff_hidden_mult=4, dropout=0.0):
        super().__init__()

        self.attention = SelfAttention(emb, heads=heads, mask=mask)
        self.mask = mask

        self.norm1 = nn.LayerNorm(emb)
        self.norm2 = nn.LayerNorm(emb)

        self.ff = nn.Sequential(
            nn.Linear(emb, ff_hidden_mult * emb),
            nn.ReLU(),
            nn.Linear(ff_hidden_mult * emb, emb)
        )

        self.do = nn.Dropout(dropout)

    def forward(self, x):
        attended = self.attention(x)
        x = self.norm1(attended + x) # Res connection
        x = self.do(x)
        fedforward = self.ff(x)
        x = self.norm2(fedforward + x) # Res connection
        x = self.do(x)
        return x


# Because here char embeddings are learned, and given also the low dimensionality of the data
# it seems weird to me to learn the positional encoding as well. Let's try with a simple
# sine and cosine embedding
class PositionalEncoding(nn.Module):

    def __init__(self, emb, seq_length=5000, dropout=0.0):
        super(PositionalEncoding, self).__init__()
        self.dropout = nn.Dropout(p=dropout)

        pe = torch.zeros(seq_length, emb)
        position = torch.arange(0, seq_length, dtype=torch.float).unsqueeze(1)
        div_term = torch.exp( torch.arange(0, emb, 2).float() * (-math.log(10000.0) / emb) )
        pe[:, 0::2] = torch.sin(position * div_term)
        pe[:, 1::2] = torch.cos(position * div_term)
        pe = pe.unsqueeze(0).transpose(0, 1)
        self.register_buffer('pe', pe)

    def forward(self, x):
        x = x + self.pe[:x.size(0), :].to(x.device)
        return self.dropout(x)
     

class GTransformer(nn.Module):
    """
    Transformer for generating text (character by character).
    """

    def __init__(self, emb, heads, depth, seq_length, num_tokens, dropout=0.0):
        super().__init__()

        self.num_tokens = num_tokens
        self.token_embedding = nn.Embedding(embedding_dim=emb, num_embeddings=num_tokens)
        # self.pos_embedding = nn.Embedding(embedding_dim=emb, num_embeddings=seq_length)
        self.pos_encoding = PositionalEncoding(emb=emb, seq_length=seq_length,dropout=dropout)

        tblocks = []
        for i in range(depth):
            tblocks.append(
                TransformerBlock(emb=emb, heads=heads, seq_length=seq_length, mask=True,dropout=dropout))

        self.tblocks = nn.Sequential(*tblocks)

        self.toprobs = nn.Linear(emb, num_tokens)

    def forward(self, x):
        """
        :param x: A batch by sequence length integer tensor of token indices.
        :return: predicted log-probability vectors for each token based on the preceding tokens.
        """
        x = self.token_embedding(x)
        b, t, e = x.size()
        #positions = self.pos_embedding(torch.arange(t, device=x.device))[None, :, :].expand(b, t, e)
        #x = tokens + positions
        x = self.pos_encoding(x)
        x = self.tblocks(x)
        x = self.toprobs(x.view(b*t, e)).view(b, t, self.num_tokens)

        return F.log_softmax(x, dim=2)

# Note that at the last layer I need a nn.Linear to go back to the num_token ( vocab_size ) dimension from the embedding_dim
# At that point, because the weights are learned, I'm good to go. An alternative would be to preserve embedding_dim = vocab_size
# all thoughout the network and at the end simply do matmul(output,vocab_embedding) [possibly transposed] to get the similarity vectors with
# the whole vocabulary embedding matrix [ self.token_embedding(vocab) to be computed once ] and that would give me the right thing
# to sstick into the softmax layer.


# -----------------------------------------------------------------------------------------------
# -----------------------------------------------------------------------------------------------
# -----------------------------------------------------------------------------------------------
# -----------------------------------------------------------------------------------------------

class DecepticonBlock(nn.Module):
    def __init__(self, emb, heads, seq_length, ff_hidden_mult=4, dropout=0.0):
        super().__init__()

        self.left_attention = SelfAttention(emb, heads=heads, mask=True)
        self.bidirectional_attention = SelfAttention(emb, heads=heads, mask=False)

        self.norm1 = nn.LayerNorm(emb)

        self.ff = nn.Sequential(
            nn.Linear(emb, ff_hidden_mult * emb),
            nn.ReLU(),
            nn.Linear(ff_hidden_mult * emb, emb)
        )

        self.norm2 = nn.LayerNorm(emb)

        self.do = nn.Dropout(dropout)

    def forward(self, x):

        la = self.left_attention(x)
        ba = self.bidirectional_attention(x)
        x = self.norm1(la + ba + x)
        x = self.do(x)

        fedforward = self.ff(x)
        x = self.norm2(fedforward + x)
        x = self.do(x)

        return x


class GDecepticon(nn.Module):
    """
    Decepticon for generating text (character by character).
    """

    def __init__(self, emb, heads, depth, seq_length, num_tokens, dropout=0.0):
        super().__init__()

        self.num_tokens = num_tokens
        self.token_embedding = nn.Embedding(embedding_dim=emb, num_embeddings=num_tokens)
        self.pos_encoding = PositionalEncoding(emb=emb, seq_length=seq_length,dropout=dropout)

        tblocks = []
        for i in range(depth):
            tblocks.append(
                DecepticonBlock(emb=emb, heads=heads, seq_length=seq_length, dropout=dropout))

        self.tblocks = nn.Sequential(*tblocks)

        self.toprobs = nn.Linear(emb, num_tokens)

    def forward(self, x):
        """
        :param x: A batch by sequence length integer tensor of token indices.
        :return: predicted log-probability vectors for each token based on the preceding tokens.
        """
        x = self.token_embedding(x)
        b, t, e = x.size()
        x = self.pos_encoding(x)
        x = self.tblocks(x)
        x = self.toprobs(x.view(b*t, e)).view(b, t, self.num_tokens)

        return F.log_softmax(x, dim=2)

